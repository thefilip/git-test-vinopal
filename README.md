## Master Branch
This is master branch of this repository.

---

## Clone a repository

1. You'll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if youâ€™d like to and then click **Clone**.
4. Open the directory you just created to see your repository's files.